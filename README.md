## HACKATHON IBGE ##


Sobre
-------

Git do desenvolvimento do projeto **una**, para o *hackathon* ibge, dado na mão, na *campus party* brasília 2017. 

Projeto desenvolvido dos dias 15 a 17 de junho de 2017.

O **una** se trata da solução que encontramos para conseguir alinhar os [17 objetivos para transformar o nosso mundo](https://nacoesunidas.org/pos2015/) , segundo a **ONU**, com o que há de dados brasileiros sobre esses objetivos. Com algumas perguntas simples, sobre o usuário do **una**, nos traçamos dados relacionados aos objetivos da **ONU** que interessam o nosso usuário, interagem e tem a ver com esse usuário. Com isso pretendemos com que os utilizadores da solução entendam melhor os dados que 

Tecnologias pretendidas até o momento
-------

 1. Wit.ai
 2. Chart.js
 3. node.js
 4. bootstrap

Dados pretendidos até o momento
-------

[Estatísticas de Gênero](http://www.ibge.gov.br/apps/snig/v1/index.html),
[Mapa da violência 2015](http://www.mapadaviolencia.org.br/pdf2015/MapaViolencia_2015_mulheres.pdf),
[Projeção da população do Brasil e das Unidades da Federação](http://www.ibge.gov.br/apps/populacao/projecao/)

Dúvidas e sugestões
-------
Esse repositório é livre, logo, pode e deve ser usado para esse tipo de coisa. Use o sistema de *issues* e abuse dos *pull requests*.

*Links*
-------
E em breve postaremos o link para acesso a página do **una**:

O bot será encontrado na nossa página do Facebook (que já existe):

[Una](https://www.facebook.com/Unadata-1255147937927099/)
